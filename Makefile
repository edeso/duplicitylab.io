# Makefile for duplicity web contents

foo = $(wildcard *.wml)
WML = $(filter-out $(wildcard *template*),$(foo))
HTML = $(WML:.wml=.html)
STABLETAG?=unset-stabletag
DEVELVERSION?=unset-develversion
VERSIONDIR=public/$(STABLETAG)/
DEVELDIR=public/devel/


%.html : %.wml template.wml
	wml -D STABLEVERSION=$(STABLETAG:rel.%=%) -D DEVELVERSION=$(DEVELVERSION) -n -q -o $@ $<

# Targets

all: $(HTML)
	make pages

clean:
	rm -rf *.html public/
	find . -name ".#*" -delete

pages:
	# make needed dirs
	mkdir -p public/contrib
	mkdir -p $(VERSIONDIR)
	ln -s ../$(VERSIONDIR) public/stable

	# get stable docs from duplicity project
	curl -q https://gitlab.com/duplicity/duplicity/-/raw/$(STABLETAG)/CHANGELOG.md --output $(VERSIONDIR)/CHANGELOG.md
	curl -q https://gitlab.com/duplicity/duplicity/-/raw/$(STABLETAG)/README.md --output $(VERSIONDIR)/README.md
	curl -q https://gitlab.com/duplicity/duplicity/-/raw/$(STABLETAG)/bin/duplicity.1 --output $(VERSIONDIR)/duplicity.1
	curl -q https://gitlab.com/duplicity/duplicity/-/raw/$(STABLETAG)/bin/rdiffdir.1 --output $(VERSIONDIR)/rdiffdir.1

	# convert stable release man/doc pages to html with toc
	pandoc -f man -t markdown $(VERSIONDIR)/duplicity.1 | pandoc -t html -f markdown -s --toc -M document-css=false --metadata title="duplicity stable version $(STABLETAG:rel.%=%) man page" > $(VERSIONDIR)/duplicity.1.html
	pandoc -r man -w html $(VERSIONDIR)/rdiffdir.1 > $(VERSIONDIR)/rdiffdir.1.html
	pandoc -r markdown -w html $(VERSIONDIR)/CHANGELOG.md > $(VERSIONDIR)/CHANGELOG.html
	pandoc -r markdown -w html $(VERSIONDIR)/README.md > $(VERSIONDIR)/README.html

	mkdir -p $(DEVELDIR)

	# get devel docs from duplicity project
	curl -q https://gitlab.com/duplicity/duplicity/-/raw/HEAD/CHANGELOG.md --output $(DEVELDIR)/CHANGELOG.md
	curl -q https://gitlab.com/duplicity/duplicity/-/raw/HEAD/README.md --output $(DEVELDIR)/README.md
	curl -q https://gitlab.com/duplicity/duplicity/-/raw/HEAD/bin/duplicity.1 --output $(DEVELDIR)/duplicity.1
	curl -q https://gitlab.com/duplicity/duplicity/-/raw/HEAD/bin/rdiffdir.1 --output $(DEVELDIR)/rdiffdir.1

	# convert stable release man/doc pages to html with toc
	pandoc -f man -t markdown $(DEVELDIR)/duplicity.1 | pandoc -t html -f markdown -s --toc -M document-css=false --metadata title="duplicity git $(DEVELVERSION) man page" > $(DEVELDIR)/duplicity.1.html
	pandoc -r man -w html $(DEVELDIR)/rdiffdir.1 > $(DEVELDIR)/rdiffdir.1.html
	pandoc -r markdown -w html $(DEVELDIR)/CHANGELOG.md > $(DEVELDIR)/CHANGELOG.html
	pandoc -r markdown -w html $(DEVELDIR)/README.md > $(DEVELDIR)/README.html

	# Make user contributions avail
	cp -rp contrib/* public/contrib/

	# Make html, css, png avail
	cp *.png style.css public
	mv *.html public

.PHONY: clean pages
