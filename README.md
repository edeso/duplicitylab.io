# README for building duplicity web


## REQUIREMENTS
* curl - https://manpages.ubuntu.com/manpages/impish/man1/curl.1.html
* make - https://manpages.ubuntu.com/manpages/impish/man1/make.1.html
* pandoc - https://manpages.ubuntu.com/manpages/impish/man1/pandoc.1.html
* wml - https://manpages.ubuntu.com/manpages/impish/man1/wml.1.html

Both are in standard Linux distros


## BUILDING
* update wml as needed
* commit to repository
* check CI/CD pages job
